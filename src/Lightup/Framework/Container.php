<?php

namespace Lightup\Framework;

use Psr\Container\ContainerInterface;

interface Container extends ContainerInterface
{
    public function executeObjectMethod(string $class, string $method, array $params = []): mixed;

    public function bind(string $class, string | object $mix): void;

    public static function getInstance(): Container;
}