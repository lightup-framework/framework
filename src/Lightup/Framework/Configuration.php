<?php

namespace Lightup\Framework;

interface Configuration
{
    public function get(string $key, mixed $default = null): mixed;
}