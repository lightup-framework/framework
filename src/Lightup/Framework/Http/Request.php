<?php

namespace Lightup\Framework\Http;

interface Request
{
    public function getMethod(): string;

    public function getPath(): string;

    public function getInput($key, string $default = ""): string;

    public function getInputs(): array;

    public function setInput(string $key, string $value): void;

    public function swap(array $inputs): void;
}