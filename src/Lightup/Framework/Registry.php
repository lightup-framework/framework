<?php

namespace Lightup\Framework;

interface Registry
{
    public function has($key);

    public function add($key, $value);

    public function get($key);

    public function remove($key);
}