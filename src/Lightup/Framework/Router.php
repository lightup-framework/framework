<?php

namespace Lightup\Framework;

interface Router
{
    public function route(): array;
}