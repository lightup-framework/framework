<?php

namespace Lightup\Framework\Validator;

abstract class Rule
{
    public abstract function message(string $attribute): string;
    public abstract function passes(string $attribute, mixed $value): bool;

    public function additional(string $param){}
}