<?php

namespace Lightup\Framework;

abstract class Middleware
{
    public abstract function handle();
}