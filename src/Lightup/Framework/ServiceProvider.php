<?php

namespace Lightup\Framework;

interface ServiceProvider
{
    public function boot(): void;
    public function shutdown(): void;
}


