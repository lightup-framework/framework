<?php

namespace Lightup\Framework;

interface Environment
{
    public function get(string $key, mixed $default = null): mixed;
}